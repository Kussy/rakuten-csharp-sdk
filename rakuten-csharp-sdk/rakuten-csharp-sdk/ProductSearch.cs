﻿using System.Collections.Generic;
using System.Linq;

namespace RakutenWebService
{
    /// <summary>商品価格ナビ製品検索API</summary>
    public sealed class ProductSearch : AbstractSearch
    {
        #region コンストラクタ(ファクトリを用意する)
        /// <summary>コンストラクタ</summary>
        /// <param name="applicationId">アプリケーションID</param>
        internal ProductSearch(string applicationId)
            : base(applicationId)
        {
            this.BaseUrl = BaseUrlConst.PRODUCT_SEARCH;
        }
        #endregion

        #region 内部メソッド
        /// <summary>商品価格ナビ製品検索APIに無効なパラメータを除く</summary>
        /// <param name="parameters">リクエストパラメータ</param>
        /// <returns>有効なリクエストパラメータ</returns>
        protected override IDictionary<string, string> removeInvalidParameters(IDictionary<string, string> parameters)
        {
            return parameters.Where(p => AllowedParameters.ProductSearch().Contains(p.Key)).ToDictionary(p => p.Key, p => p.Value);
        }
        #endregion
    }
}
