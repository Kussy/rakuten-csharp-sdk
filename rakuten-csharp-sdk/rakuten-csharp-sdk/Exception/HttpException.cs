﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace RakutenWebService
{
    /// <summary>HTTP 要求の処理中に発生した例外</summary>
    [SerializableAttribute]
    public class HttpException : Exception
    {
        #region コンストラクタ
        /// <summary>コンストラクタ</summary>
        /// <param name="statusCode">ステータスコード</param>
        public HttpException(HttpStatusCode statusCode)
        {
            this.StatusCode = statusCode;
        }

        /// <summary>継承用コンストラクタ</summary>
        /// <param name="info">SerializationInfo</param>
        /// <param name="context">StreamingContext</param>
        protected HttpException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            StatusCode = (HttpStatusCode)info.GetValue("StatusCode", typeof(HttpStatusCode));
        }
        #endregion

        #region プロパティ
        /// <summary>HTTP 要求のステータスコード</summary>
        public HttpStatusCode StatusCode { get; protected set; }
        #endregion

        #region オーバーライド
        /// <summary>シリアライズのために追加した属性をSerializationInfoに追加する</summary>
        /// <param name="info">SerializationInfo</param>
        /// <param name="context">StreamingContext</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("StatusCode", this.StatusCode);
        }
        #endregion
    }
}