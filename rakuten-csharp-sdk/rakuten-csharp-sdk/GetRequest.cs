﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace RakutenWebService
{
    /// <summary>チェック済リクエストパラメータを使ってRESTのGETリクエストを作成</summary>
    internal class GetRequest
    {
        /// <summary>ベースURL</summary>
        private readonly string baseUrl;
        /// <summary>クエリストリング</summary>
        private readonly string queryString;

        /// <summary>コンストラクタ</summary>
        /// <param name="baseUrl">ベースURL</param>
        /// <param name="applicationId">アプリケーションID</param>
        /// <param name="parameters">リクエストパラメータ</param>
        internal GetRequest(string baseUrl, string applicationId, IDictionary<string, string> parameters)
        {
            this.baseUrl = baseUrl;
            this.queryString = string.Format("?applicationId={0}{1}", applicationId, createQueryString(parameters));
        }

        /// <summary>GETリクエストのURL</summary>
        /// <returns>URL</returns>
        internal string Url()
        {
            return this.baseUrl + this.queryString;
        }

        /// <summary>リクエストパラメータからクエリストリングを作成</summary>
        /// <param name="parameters">リクエストパラメータ</param>
        /// <returns>URLエンコード済のクエリストリング</returns>
        private string createQueryString(IDictionary<string, string> parameters) 
        {
            var encodedParameters = urlEncode(parameters);
            var sb = new StringBuilder();
            encodedParameters.ToList().ForEach(p => sb.AppendFormat("&{0}={1}", p.Key, p.Value));
            return sb.ToString();
        }

        /// <summary>全要素をURLエンコードする</summary>
        /// <param name="parameters">リクエストパラメータ</param>
        /// <returns>URLエンコード済リクエストパラメータ</returns>
        private IDictionary<string,string> urlEncode(IDictionary<string,string> parameters)
        {
            var result = new Dictionary<string,string>();
            parameters.ToList().ForEach(p => result.Add(urlEncode(p.Key), urlEncode(p.Value)));
            return result;
        }

        /// <summary>UTF-8でURLエンコードする</summary>
        /// <param name="target">エンコード対象の文字列</param>
        /// <returns>エンコード済文字列</returns>
        private string urlEncode(string target)
        {
            return HttpUtility.UrlEncode(target, Encoding.UTF8);

        }
    }
}
