﻿using Codeplex.Data;
using System.Linq;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace RakutenWebService
{
    /// <summary>楽天ジャンル検索APIの振舞い</summary>
    [TestClass]
    public class IchibaGenreSearchBehavior
    {
        #region Fields
        /// <summary>楽天ジャンル検索API</summary>
        private AbstractSearch searcher;
        #endregion

        #region Properties
        /// <summary>
        /// <para>テストコンテキスト</para>
        /// <para>パラメトライズテストに必要</para>
        /// </summary>
        public TestContext TestContext { get; set; }
        #endregion

        #region Arrangements
        /// <summary>テストメソッド実行前のセットアップ</summary>
        [TestInitialize]
        public void SetUp()
        {
            searcher = new IchibaGenreSearch(Helper.APPLICATION_ID);
        }
        #endregion

        #region Behavior
        #region case of unregistered application id.
        [TestMethod]
        public void 登録されていないアプリケーションIDで検索すると400エラーを返すべき()
        {
            var ex = AssertEx.Throws<HttpException>(() => unregisteredSercher().Search(rootGenre()));
            ex.StatusCode.Is(HttpStatusCode.BadRequest);
        }
        #endregion

        #region case of bad parameters.
        [TestMethod]
        public void 存在しないジャンルIDで検索すると400エラーを返すべき()
        {
            var ex = AssertEx.Throws<HttpException>(() => searcher.Search(unexistGenre()));
            ex.StatusCode.Is(HttpStatusCode.BadRequest);
        }
        #endregion

        #region case of some search results.
        [TestMethod]
        public void ジャンルIDが存在するなら自分自身を取得できるべき()
        {
            var json = DynamicJson.Parse(searcher.Search(rootGenre()));
            ((double)json.current.genreId).Is(0);
        }
        #endregion
        #endregion

        #region TestParameters
        private AbstractSearch unregisteredSercher()
        {
            return new IchibaGenreSearch("UnregisterdId"); ;
        }
        
        private IDictionary<string, string> rootGenre()
        {
            return Helper.parameters(Helper.entry("genreId", "0"));
        }

        private IDictionary<string, string> unexistGenre()
        {
            return Helper.parameters(Helper.entry("genreId", "unexistId"));
        }

        #endregion
    }
}