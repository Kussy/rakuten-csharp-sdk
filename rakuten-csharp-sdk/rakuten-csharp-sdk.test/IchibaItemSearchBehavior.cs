﻿using Codeplex.Data;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace RakutenWebService
{
    /// <summary>楽天商品検索APIの振舞い</summary>
    [TestClass]
    public class IchibaItemSearchBehavior
    {
        #region Fields
        /// <summary>楽天商品検索API</summary>
        private AbstractSearch searcher;
        #endregion

        #region Properties
        /// <summary>
        /// <para>テストコンテキスト</para>
        /// <para>パラメトライズテストに必要</para>
        /// </summary>
        public TestContext TestContext { get; set; }
        #endregion

        #region Arrangements
        /// <summary>テストメソッド実行前のセットアップ</summary>
        [TestInitialize]
        public void SetUp()
        {
            searcher = new IchibaItemSearch(Helper.APPLICATION_ID);
        }
        #endregion

        #region Behavior
        #region case of unregistered application id.
        [TestMethod]
        public void 登録されていないアプリケーションIDでJSON要求を送ると400エラーを返すべき()
        {
            var ex = AssertEx.Throws<HttpException>(() => unregisteredSercher().Search(expectedSomeResult()));
            ex.StatusCode.Is(HttpStatusCode.BadRequest);
        }
        #endregion

        #region case of no search results.
        /// <summary>公式では404エラーと書いているが実態は違った</summary>
        [TestMethod]
        public void 対象のデータが存在しなかった場合jsonのcountが0であるべき()
        {
            var json = searcher.Search(expectedEmptyResult());
            ((double)DynamicJson.Parse(json).count).Is(0);
        }
        #endregion

        #region case of some search results.
        [TestMethod]
        public void 取りあえず検索成功しそうな場合jsonのcountが0以外であるべき()
        {
            var json = searcher.Search(expectedSomeResult());
            ((double)DynamicJson.Parse(json).count).IsNot(0);
        }
        #endregion
        #endregion

        #region TestParameters
        private AbstractSearch unregisteredSercher()
        {
            return new IchibaItemSearch("UnregisterdId"); ;
        }

        private IDictionary<string, string> expectedSomeResult()
        {
            return Helper.parameters(Helper.entry("keyword", "test"));
        }
        private IDictionary<string, string> expectedEmptyResult()
        {
            return Helper.parameters(Helper.entry("keyword", "qwertyuiop"), Helper.entry("minPrice", "999999999"));
        }
        #endregion
    }
}